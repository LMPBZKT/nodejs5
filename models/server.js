const express = require('express');

class Server {

  constructor() {
    this.app = express();
    this.port = process.env.PORT || 8080;
    this.datePath = "/api/hora"

    this.middlewares()
    this.routes();
  }

  middlewares() {
    this.app.use(express.json())
  }

  routes() {
    this.app.use(this.datePath, require('../routes/datos'))

  }

  listen() {
    this.app.listen(this.port, () => {
      console.log('Server running on the port', this.port);
    });
  }
  
}

module.exports = Server