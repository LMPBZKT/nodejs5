const {Router} = require('express')

const {
  dataGet
} = require('../controllers/datos1')

const route = Router()
route.get('/', dataGet)

module.exports = route